
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <opencv2/opencv.hpp>
#include<iostream>
#include<cmath>
#include <stdio.h>
#define THREADS_PER_BLOCK 512

using namespace std;
using namespace cv;


int* rgb(Mat A, int cols, int rows);
int* cut_values(int* A, const int rows, const int cols);
int* rescale_values(int* A, const int rows, const int cols);
uchar* create_bgr_matrix(uchar* A, int* r, int* g, int* b, const int rows, const int cols);
cudaError_t GPU_function(int* r, int* g, int* b, int* rout, int* gout, int* bout, const int rows, const int cols, int* filter);

__global__ void convolution(int *in, int *out, int *filter, const int rows, const int cols)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int sum = 0;
    //WARUNEK POZOSTANIA W ZAKRESIE WIELKO�CI MACIERZY OBRAZU
    if (idx < rows * cols) 
    {
        //WYZNACZAMY WSPӣRZ�DNE PIXELA DLA KT�REGO PRZEPROWADZAMY OBLICZENIA
        int x_pixel = int(idx / cols);
        int y_pixel = idx - x_pixel*cols;
        //WARUNEK BRZEGOWY
        if (x_pixel < rows - 1 && y_pixel < cols - 1 && x_pixel > 0 && y_pixel > 0) 
        {
            for (int k = -1; k <= 1; k++) {
                for (int j = -1; j <= 1; j++) {
                    sum = sum + filter[(j + 1) * 3 + k + 1] * in[(x_pixel - j) * cols + y_pixel - k];
                }
            }
            out[x_pixel * cols + y_pixel] = sum;

        }
        else 
        {
            out[x_pixel * cols + y_pixel] = in[x_pixel * cols + y_pixel];
        }
    }
}


int main()
{
   //WCZYTUJEMY OBRAZ ORAZ GO WY�WIETLAMY
    Mat img = imread("messi.jpg", IMREAD_COLOR);
    const int rows = img.rows;
    const int cols = img.cols;
    namedWindow("Orginal Image", WINDOW_NORMAL);
    imshow("Orginal Image", img);
    waitKey(0);
    //TWORZYY MACIERZ TYPU Mat i rozdzielamy na ka�dy kolor R, G i B
    Mat img_rgb[3];
    split(img, img_rgb);
    //TWORZYMY WSKA�NIKI TYPU INT PRZECHOWUJ�CE WARTO�CI PIXELI POSZCZEG�LNYCH KOLOR�W
    int* img_b = rgb(img_rgb[0], cols, rows);
    int* img_g = rgb(img_rgb[1], cols, rows);
    int* img_r = rgb(img_rgb[2], cols, rows);

    //DEKLARUJEMY KERNELE FILTR�W
    int sobel_filter[] = {1, 2, 1, 0, 0, 0, -1, -2, -1}; //Poziomy filtr Sobel'a - wykrywa kraw�dzie poziome. Obci�cie warto�ci pixeli. 
    int hp_filter[] = {-1, -1, -1, -1, 9, -1, -1, -1, -1}; //Jego u�ycie powoduje znaczne wyostrzenie obrazu, ale tak�e wzmocnienie wszelkich szum�w i zak��ce�. Obci�cie warto�ci pixeli. 
    int lp_filter[] = {1, 1, 1, 1, 4, 1, 1, 1, 1}; //Jego u�ycie powoduje zredukowanie szum�w ale r�wnie� wyg�adzenie i rozmycie obrazu. Przeskalowanie pixeli.
    int embosse_filter[] = {1, 1, 0, 1, 1, -1, -0, -1, -1}; //Filtr uwypuklaj�cy p�nocny-zach�d. Obci�cie warto�ci pixeli.

    //TWORZYMY WSKA�NIKI DLA POSZCZEG�LNYCH FILT�RW DLA KAZDEGO Z KOLOR�W
    int* img_r_hp = new int[cols * rows];
    int* img_g_hp = new int[cols * rows];
    int* img_b_hp = new int[cols * rows];
    int* img_r_sobel = new int[cols * rows];
    int* img_g_sobel = new int[cols * rows];
    int* img_b_sobel = new int[cols * rows];
    int* img_r_lp = new int[cols * rows];
    int* img_g_lp = new int[cols * rows];
    int* img_b_lp = new int[cols * rows];
    int* img_r_embosse = new int[cols * rows];
    int* img_g_embosse = new int[cols * rows];
    int* img_b_embosse = new int[cols * rows];

    //WYWO�UJEMY FUNKCJE SPLOTU NA GPU DLA PIERWSZEGO FILTRA
    cudaError_t cudaStatus = GPU_function(img_r, img_g, img_b, img_r_hp, img_g_hp, img_b_hp, rows, cols, hp_filter);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addWithCuda failed!");
        return 1;
    }
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
    //OBCINAMY WARTO�CI PIXELI TAK ABY BY�Y OD 0 DO 255
    img_b_hp = cut_values(img_b_hp, rows, cols);
    img_g_hp = cut_values(img_g_hp, rows, cols);
    img_r_hp = cut_values(img_r_hp, rows, cols);




    //WYWO�UJEMY FUNKCJE SPLOTU NA GPU DLA DRUGIEGO FILTRA
    cudaStatus = GPU_function(img_r, img_g, img_b, img_r_sobel, img_g_sobel, img_b_sobel, rows, cols, sobel_filter);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addWithCuda failed!");
        return 1;
    }
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
    //OBCINAMY WARTO�CI PIXELI TAK ABY BY�Y OD 0 DO 255
    img_b_sobel = cut_values(img_b_sobel, rows, cols);
    img_g_sobel = cut_values(img_g_sobel, rows, cols);
    img_r_sobel = cut_values(img_r_sobel, rows, cols);




    //WYWO�UJEMY FUNKCJE SPLOTU NA GPU DLA TRZECIEGO FILTRA
    cudaStatus = GPU_function(img_r, img_g, img_b, img_r_lp, img_g_lp, img_b_lp, rows, cols, lp_filter);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addWithCuda failed!");
        return 1;
    }
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
    //PRZESKALOWUJEMY WARTO�CI PIXELI TAK ABY BY�Y OD 0 DO 255
    img_b_lp = rescale_values(img_b_lp, rows, cols);
    img_g_lp = rescale_values(img_g_lp, rows, cols);
    img_r_lp = rescale_values(img_r_lp, rows, cols);


    //WYWO�UJEMY FUNKCJE SPLOTU NA GPU DLA CZWARTEGO FILTRA
    cudaStatus = GPU_function(img_r, img_g, img_b, img_r_embosse, img_g_embosse, img_b_embosse, rows, cols, embosse_filter);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addWithCuda failed!");
        return 1;
    }
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }
    //PRZESKALOWUJEMY WARTO�CI PIXELI TAK ABY BY�Y OD 0 DO 255
    img_b_embosse = cut_values(img_b_embosse, rows, cols);
    img_g_embosse = cut_values(img_g_embosse, rows, cols);
    img_r_embosse = cut_values(img_r_embosse, rows, cols);


    ////TWORZYMY WSKA�NIKI TYPU UCHAR DLA , KT�RE B�D� PRZECHOWYWA� WARTO�CI PIXELI WSZYSTKICH KOLOR�W
    uchar* img_bgr_hp = new uchar[3 * rows * cols];
    uchar* img_bgr_sobel = new uchar[3 * rows * cols];
    uchar* img_bgr_lp = new uchar[3 * rows * cols];
    uchar* img_bgr_embosse = new uchar[3 * rows * cols];

    //WYWO�UJEMY FUNKCJE KT�RA W ODPOWIEDNIEJ KOLEJNO�CI WPISZE WARTO�CI POSZCZEG�LNYCH KOLOR�W(B G R, B G R...) DO MACIERZY TYPU UCHAR,
    //TAK ABY Z �ATWO�CI� MO�NA BY�O J� PRZEKONWERTOWA� DO POSTACI MACIERZY TYPU Mat
    img_bgr_hp = create_bgr_matrix(img_bgr_hp, img_r_hp, img_g_hp, img_b_hp, rows, cols);
    img_bgr_sobel = create_bgr_matrix(img_bgr_sobel, img_r_sobel, img_g_sobel, img_b_sobel, rows, cols);
    img_bgr_lp = create_bgr_matrix(img_bgr_lp, img_r_lp, img_g_lp, img_b_lp, rows, cols);
    img_bgr_embosse = create_bgr_matrix(img_bgr_embosse, img_r_embosse, img_g_embosse, img_b_embosse, rows, cols);

    //KONWERTUJEMY DANE DO MACIERZY TYPU Mat ORAZ WY�WIETLAMY WYNIKI DZIA�ANIA POSZCZEG�LNYCH FILTR�W
    Mat HP = Mat(rows, cols, CV_8UC3, img_bgr_hp);
    namedWindow("HP FILTER", WINDOW_NORMAL);
    imshow("HP FILTER", HP);

    Mat SOBEL = Mat(rows, cols, CV_8UC3, img_bgr_sobel);
    namedWindow("SOBEL FILTER", WINDOW_NORMAL);
    imshow("SOBEL FILTER", SOBEL);

    Mat LP = Mat(rows, cols, CV_8UC3, img_bgr_lp);
    namedWindow("LP FILTER", WINDOW_NORMAL);
    imshow("LP FILTER", LP);

    Mat EMBOSSE = Mat(rows, cols, CV_8UC3, img_bgr_embosse);
    namedWindow("EMBOSSING FILTER", WINDOW_NORMAL);
    imshow("EMBOSSING FILTER", EMBOSSE);
    waitKey(0);

    return 0;
}

int* rgb(Mat A, int cols, int rows) {
    int* B = new int[cols * rows];
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            B[i * cols + j] = int(A.at<uchar>(i, j));
        }

    }
    return B;
}

int* cut_values(int* A, const int rows, const int cols)
{
    for (int i = 0; i < rows * cols; i++) {
        if (A[i] < 0) {
            A[i] = 0;
        }

        if (A[i] > 255) {
            A[i] = 255;
        }
    }
    return A;
}

int* rescale_values(int* A, const int rows, const int cols)
{
    int max = 0;
    int min = 0;
    for (int i = 0; i < rows * cols; i++)
    {
        if (A[i] > max) {
            max = A[i];
        }
        if (A[i] < min) {
            min = A[i];
        }
    }
    max -= min;
    for (int i = 0; i < rows * cols; i++)
    {
        A[i] = 255 * (A[i] - min) / max;
    }
    return A;
}

cudaError_t GPU_function(int *r,int *g,int *b, int* rout, int* gout, int* bout, const int rows, const int cols, int *filter)
{
    int *dev_r = 0;
    int *dev_g = 0;
    int *dev_b = 0;
    int* dev_out_r = 0;
    int* dev_out_g = 0;
    int* dev_out_b = 0;
    int* dev_filter = 0;
    cudaError_t cudaStatus;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    // Allocate GPU buffers for seven vectors (four input, three output)    .
    cudaStatus = cudaMalloc((void**)&dev_r, rows*cols * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_g, rows*cols * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_b, rows*cols * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_out_r, rows * cols * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_out_g, rows * cols * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_out_b, rows * cols * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }


    cudaStatus = cudaMalloc((void**)&dev_filter, 9 * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    cudaStatus = cudaMemcpy(dev_r, r, rows*cols * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_g, g, rows*cols * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_b, b, rows * cols * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_filter, filter, 9 * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    int num_of_blocks = ((rows * cols) / THREADS_PER_BLOCK) + 1;
    // Launch a kernel on the GPU with one thread for each element.
    convolution << <num_of_blocks, THREADS_PER_BLOCK >> > (dev_r, dev_out_r, dev_filter, rows, cols);
    convolution << <num_of_blocks, THREADS_PER_BLOCK >> > (dev_g, dev_out_g, dev_filter, rows, cols);
    convolution << <num_of_blocks, THREADS_PER_BLOCK >> > (dev_b, dev_out_b, dev_filter, rows, cols);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(rout, dev_out_r, rows*cols * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }
    cudaStatus = cudaMemcpy(gout, dev_out_g, rows*cols * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }
    cudaStatus = cudaMemcpy(bout, dev_out_b, rows*cols * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_r);
    cudaFree(dev_g);
    cudaFree(dev_b);
    cudaFree(dev_out_r);
    cudaFree(dev_out_g);
    cudaFree(dev_out_b);
    
    return cudaStatus;
}

uchar* create_bgr_matrix(uchar* A, int* r, int* g, int* b, const int rows, const int cols)
{
    for (int i = 0; i < 3 * cols * rows; i++)
    {
        if (i % 3 == 0)
            A[i] = b[int(i / 3)];
        else if (i % 3 == 1)
            A[i] = g[int(i / 3)];
        else
            A[i] = r[int(i / 3)];
    }
    return A;
}